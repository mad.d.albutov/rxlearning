//
//  SectionItem.swift
//  RXTask
//
//  Created by Данил Албутов on 20.05.2022.
//

import RxDataSources

struct SectionItem: IdentifiableType, Equatable {
    
    typealias Identity = String
    
    var identity: String
    var value: String
    
    
}
