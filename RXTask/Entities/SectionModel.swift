//
//  SectionModel.swift
//  RXTask
//
//  Created by Данил Албутов on 20.05.2022.
//

import Foundation
import Differentiator
import RxDataSources

struct SectionModel{
    var identity: Int
    var items: [TableItemModel]
}

extension SectionModel: AnimatableSectionModelType {
    init(original: SectionModel, items: [TableItemModel]) {
        self = original
        self.items = items
    }
}
