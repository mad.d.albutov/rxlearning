//
//  TableItemModel.swift
//  RXTask
//
//  Created by Данил Албутов on 24.05.2022.
//

import RxDataSources
import RxSwift
struct TableItemModel {
    
    let key: String
    let value: Observable<String>
    
    static func == (lhs: TableItemModel, rhs: TableItemModel) -> Bool {
        lhs.key == rhs.key
    }
    
}

extension TableItemModel: Equatable, IdentifiableType  {
    var identity: String {
        self.key
    }
}
