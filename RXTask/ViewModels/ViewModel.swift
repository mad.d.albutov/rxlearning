//
//  ViewModel.swift
//  RXTask
//
//  Created by Данил Албутов on 19.05.2022.
//

import RxCocoa
import RxSwift

class ViewModel {
    
    func transform(input: Input) -> Output {
        return Output(
            items: infiniteObservable(with: 10)
        )
    }
    
    func infiniteObservable(with delay: Int) -> Observable<[ItemModel]> {
        let infiniteSeq = InfiniteSequence<Observable<[ItemModel]>>(
            repeatedValue: generateItemModels().delaySubscription(.microseconds(10), scheduler: MainScheduler.instance) //временныйкостыль
        )
        
        let timerObservable = Observable<Int>
            .timer(.seconds(0), period: .seconds(delay), scheduler: MainScheduler.asyncInstance)
        
        let itemsObservable = Observable.concat(infiniteSeq) 
        
        return .zip(itemsObservable, timerObservable) { itemModels, _ in
            return itemModels
        }
    }
    
    private func generateItemModels() -> Observable<[ItemModel]> {
        .create { observer in
            
            let keys = (1...100).map {
                "Key #\($0)"
            }
            
            let models: [ItemModel] = keys.map { key in
                let value = String(Int.random(in: 0...100))
                return ItemModel(key: key, value: value)
            }
            
            observer.onNext(models)
            observer.onCompleted()
            
            return Disposables.create()
        }
    }
}

struct Input {
    
}

struct Output {
    let items: Observable<[ItemModel]>
}

struct InfiniteSequence<Element> : Sequence {
    typealias Iterator = AnyIterator<Element>
    
    private let repeatedValue: Element
    
    init(repeatedValue: Element) {
        self.repeatedValue = repeatedValue
    }
    
    func makeIterator() -> Iterator {
        let repeatedValue = self.repeatedValue
        return AnyIterator { repeatedValue }
    }
}
