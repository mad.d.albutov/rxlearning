//
//  ViewController.swift
//  RXTask
//
//  Created by Данил Албутов on 25.04.2022.

import UIKit
import RxCocoa
import RxSwift
import SnapKit
import RxDataSources

class ViewController: UIViewController {
    
    private let disposeBag = DisposeBag()
    private var connectableDisposeBag = DisposeBag()
    private let viewModel: ViewModel
    
    
    private var subjectDictionary = [String : BehaviorSubject<String>]()
    
    private var dataSource: RxTableViewSectionedAnimatedDataSource<SectionModel>!
    
    private let tableView = UITableView()
    
    init(viewModel: ViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("start--")
        configureViews()        
        configureDataSource()
        bind()
    }
    
    private func bind() {
        
        let input = Input()
        
        let output = viewModel.transform(input: input)
        
            
        let groupedObservables = output.items
            .debug("🟢")
            .flatMapLatest { items -> Observable<ItemModel> in
                .from(items)
            }
            .groupBy { arg in
                arg.key
            }
            .scan([(key: String, valueObservable: Observable<String>)]()) { arr, element in
                arr + [(element.key, element.map { $0.value })]
            }
            .do(
                onNext: { [weak self] args in
                    guard let self = self else {
                        return
                    }
                    
                    args.forEach { key, value in
                        value.share(replay: 2, scope: .whileConnected).publish().connect().disposed(by: self.connectableDisposeBag)

                    }
                    
                },
                onCompleted: { [weak self] in
                    self?.connectableDisposeBag = DisposeBag()
                }
            )
        
        Observable
            .combineLatest(output.items, groupedObservables)
            .map { items, groupedObservables in
                let dataItems: [TableItemModel] = items.compactMap { item in
                    guard let valueObservable = (groupedObservables.first { $0.key == item.key })?.valueObservable else {
                        return nil
                    }
                    
                    return TableItemModel(key: item.key, value: valueObservable)     //---------------
                }
                
                return [SectionModel(identity: 0, items: dataItems)]
            }
//            .debug("🔰")
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
    }
    
    private func configureDataSource() {
        tableView.register(TableViewCell.self, forCellReuseIdentifier: "cell")
        let dataSource = RxTableViewSectionedAnimatedDataSource<SectionModel>(
            animationConfiguration: AnimationConfiguration(
                insertAnimation: .top,
                reloadAnimation: .right,
                deleteAnimation: .right
            ),
            configureCell: { dataSource, tableView, indexPath, item in
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
                cell.selectionStyle = .none
                cell.configureCell(tableItem: item)
                return cell
            }
        )
        
        self.dataSource = dataSource
    }
    
    private func configureViews() {
        view.addSubview(tableView)
        
        tableView.snp.makeConstraints { make in
            make.edges.equalTo(view.safeAreaLayoutGuide)
        }
    }
}

