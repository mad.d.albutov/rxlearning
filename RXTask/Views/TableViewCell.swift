//
//  TableViewCell.swift
//  RXTask
//
//  Created by Данил Албутов on 24.05.2022.
//

// TODO: prepareToReuse() -> (подписки)

import Foundation
import UIKit
import RxSwift
import SnapKit
class TableViewCell: UITableViewCell {
    
    private var connectableDisposeBag = DisposeBag()
    private var disposeBag = DisposeBag()
    
    private let title = UILabel()
    private let subTitle = UILabel()
    
    private let subj = BehaviorSubject(value: "")
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        title.text = nil
        subTitle.text = nil
        
        disposeBag = DisposeBag()
    }
    
    func configureCell(tableItem: TableItemModel) {
        title.text = tableItem.key
        
        tableItem.value
            .debug("❇️ObservableVal")
            .asObservable()
            .bind(to: self.subTitle.rx.text)
            .disposed(by: disposeBag)
    }
    
    private func setupViews() {
        contentView.addSubview(title)
        title.setContentHuggingPriority(.defaultLow, for: .horizontal)
        subTitle.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        contentView.addSubview(subTitle)
        
        title.snp.makeConstraints { make in
            make.top.left.bottom.equalToSuperview().inset(5)
            make.height.equalTo(40)
        }
        
        subTitle.snp.makeConstraints { make in
            make.left.equalTo(title.snp.right).inset(5)
            make.right.top.bottom.equalToSuperview().inset(5)
        }
    }    
}
