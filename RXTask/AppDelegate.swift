//
//  AppDelegate.swift
//  RXTask
//
//  Created by Данил Албутов on 25.04.2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow()
        let vm = ViewModel()
        let vc = ViewController(viewModel: vm)
        window.rootViewController = vc
        window.makeKeyAndVisible()
        self.window = window
        return true
    }

}

